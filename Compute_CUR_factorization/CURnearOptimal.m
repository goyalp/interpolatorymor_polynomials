
% linear kernel for GR data set with target rank = 10
clear all
close all

%in.A = read_snap_data('CA-GrQc.txt');
load MatrixForCUR.mat
in.A = T_full';

in.k = 10;
in.p = 10;
in.q = 10;

in.sigma_k = 1;
in.froerr = 1;
in.froerr_k = 1;
in.specerr = 1;
in.specerr_k = 1;

r_values = (40:20:140);
c_values = (40:10:100);

in.adaptive = 0;

i = 1;

in.c = r_values(i);
in.r = c_values(i);

% output = subspace_expected(in);
output = near_optimal(in);
norm(T_full'-output.CUR)/norm(T_full')

C = output.col;
R = output.row;
[Uc,Sc,Vc] = svd(C);
[Ur,Sr,Vr] = svd(R);
tol = 1e-6;
indC = find(sum(Sc)/Sc(1,1)<tol);
indR = find(sum(Sr,2)/Sr(1,1)<tol);

Uc1 = Uc(:,1:indC);
Sc1 = Sc(1:indC,1:indC);
Vc1 = Vc(:,1:indC);

Ur1 = Ur(:,1:indR);
Sr1 = Sr(1:indR,1:indR);
Vr1 = Vr(:,1:indR);

U = Vc1*inv(Sc1)*Uc1'*T_full'*Vr1*inv(Sr1)*Ur1';
norm(T_full'-C*U*R)/norm(T_full')
