function [E,A,H,N,B,C] = ChafeeInfante_Matrices_zero_ic(k,L)

h = L/(k+1);
E = speye(2*k);
A = sparse(2*k,2*k);
H = sparse(2*k,4*k^2);
N = sparse(2*k,2*k);
B = sparse(2*k,1);
C = sparse(1,2*k);

% left boundary
A(1,1) = 1-2/h^2;
A(1,2) = 1/h^2;
A(k+1,k+1) = 2-4/h^2;

% right boundary 
A(k,k-1) = 1/h^2;
A(k,k) = 1-1/h^2;
A(2*k,2*k) = 2-2/h^2;

for i = 2:k-1
   A(i,i-1) = 1/h^2;
   A(i,i) = 1-2/h^2;
   A(i,i+1) = 1/h^2;
   
   A(k+i,k+i) = 2-4/h^2;
end

% left boundary
H(1,k+1) = -0.5;
H(1,2*k^2+1) = -0.5;
H(k+1,2) = 1/h^2;
H(k+1,2*k+1) = 1/h^2;
H(k+1,2*k^2+k+1) = -2;

% right boundary
H(k,(k-1)*2*k+2*k) = -1/2;
H(k,(2*k-1)*2*k+k) = -1/2;
H(2*k,4*k^2) = -2;
H(2*k,(k-1)*2*k+k-1) = 1/h^2;
H(2*k,(k-2)*2*k+k) = 1/h^2;

for i = 2:k-1
   H(i,(i-1)*2*k+k+i) = -0.5;
   H(i,(i+k-1)*2*k+i) = -0.5;
   H(k+i,(i-1)*2*k+i-1) = 1/h^2;
   H(k+i,(i-2)*2*k+i) = 1/h^2;
   H(k+i,(i-1)*2*k+i+1) = 1/h^2;
   H(k+i,i*2*k+i) = 1/h^2;
   H(k+i,(k+i-1)*2*k+k+i) = -2;
end    

N(k+1,1) = 2/h^2;
B(1,1) = 1/h^2;
C(1,k) = 1;