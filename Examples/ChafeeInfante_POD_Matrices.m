function [A,B] = ChafeeInfante_POD_Matrices(k,L)

h = L/(k+1);

A(1,1) = 1-2/h^2;
A(1,2) = 1/h^2;

A(k,k-1) = 1/h^2;
A(k,k) = 1-1/h^2;

for i = 2:k-1
   A(i,i-1) = 1/h^2;
   A(i,i) = 1-2/h^2;
   A(i,i+1) = 1/h^2;
end

B=zeros(k,1);
B(1,1) = 1/h^2;