close all
clc
clear all

k = 500;
r = 10;
L = 1;
h = L/(k+1);

t = linspace(0,10,500);

[E,A,H,N,b,c] = ChafeeInfante_Matrices_zero_ic(k,L);
n = size(A,1);

w1 = @(t) (1+cos(pi*t/2));
w2 = @(t) 30*(1+sin(pi*t/2));

dxnl1 = @(t,x,A,B) A*x + B*w1(t) - fcubic(x);
dxnl2 = @(t,x,A,B) A*x + B*w2(t) - fcubic(x);

dx1 = @(t,x,A,H,N,b) A*x + H*kron(x,x) + N*x*w1(t) + b*w1(t);
dx2 = @(t,x,A,H,N,b) A*x + H*kron(x,x) + N*x*w2(t) + b*w2(t);

dx_pod1 = @(t,x,A,B,V) A*x + B*w1(t) - V'*fcubic(V*x);
dx_pod2 = @(t,x,A,B,V) A*x + B*w2(t) - V'*fcubic(V*x);

[Ad,Bd] = ChafeeInfante_POD_Matrices(k,L);

options = odeset('RelTol',1e-8,'AbsTol',1e-12);
tic
[~,xnl_cos] = ode15s(dxnl1,t,sparse(k,1),options,Ad,Bd);
sim_t_orig_cos = toc; 
ynl_cos = c(1:k)*xnl_cos';

tic
[~,xnl_sin] = ode15s(dxnl2,t,sparse(k,1),options,Ad,Bd);
sim_t_orig_sin = toc; 
ynl_sin = c(1:k)*xnl_sin';

plot(t,ynl_cos,'k')
hold on
figure(2)
plot(t,ynl_sin,'k')
hold on


H = Symmetrizing_Hessian(H,n);


%% Compute two-sided projection
S = rand(r,1); maxiter = 50; tol = 1e-3; gamma = 5e-4; 
[~, Ar, Hr, Nr, Br, Cr, S1, V] = optimal_QBDAE_Truncated_modified(E,A,H,N,b,c,r,maxiter,tol,1e-2,S);
[~,xr_cos] = ode15s(dx1,t,zeros(r,1),options,Ar,Hr,Nr,Br);
yr_cos = Cr*xr_cos';
  
[~,xr_sin] = ode15s(dx2,t,zeros(r,1),options,Ar,Hr,Nr,Br);
yr_sin = Cr*xr_sin';

figure(1)
plot(t,yr_cos,'r')
figure(2)
plot(t,yr_sin,'r')
figure(3)
semilogy(t,abs((ynl_cos-yr_cos)),'r')
hold on
figure(4)
semilogy(t,abs((ynl_sin-yr_sin)),'r')
hold on


%% Compute POD reduced-order model by solution of first input
[Up,Dp,Vp] = svd(xnl_cos',0);
Up = Up(:,1:r);
Ar = Up'*Ad*Up;
Br = Up'*Bd;

optionsr = odeset('RelTol',1e-8,'AbsTol',1e-12);

tic
[~,xpod_cos] = ode15s(dx_pod1,t,zeros(r,1),optionsr,Ar,Br,Up);
ypod_cos = c(1:k)*Up*xpod_cos';
  

[~,xpod_sin] = ode15s(dx_pod2,t,zeros(r,1),optionsr,Ar,Br,Up);
ypod_sin = c(1:k)*Up*xpod_sin';

figure(1)
plot(t,ypod_cos,'g')
figure(2)
plot(t,ypod_sin,'g')
figure(3)
semilogy(t,abs((ynl_cos-ypod_cos)),'g')
figure(4)
semilogy(t,abs((ynl_sin-ypod_sin)),'g')