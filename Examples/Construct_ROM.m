function [Er,Ar,Hr,Nr,Br,Cr] = Construct_ROM(E,A,H,N,B,C,V,W)

n = size(A,1);
nr = size(V,2);

Er = W'*E*V;
Ar = Er\(W'*A*V);
Nr = Er\(W'*N*V);
Br = Er\(W'*B);
Cr = C*V;

Y1 = W'*H;
Y = ndSparse(full(Y1),[nr,n,n]);
Z2 = V'*(reshape(permute(Y,[2 1 3]),n,nr*n));
Z = ndSparse(full(Z2),[nr,nr,n]);
H3 = V'*(reshape(permute(Z,[3 2 1]),n,nr*nr));
Hr = reshape(permute(full(H3),[2 1 3]),nr,nr^2);
%keyboard
Hr = Er\Hr;
Er = speye(nr);
