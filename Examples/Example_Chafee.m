close all
clearvars
clc

k = 500;
l = 1;
rng(1)
[E,A,H,N,b,c] = ChafeeInfante_Matrices_zero_ic(k,l);
[Ad,Bd] = ChafeeInfante_POD_Matrices(k,l);

t = linspace(0,4,500);
% input 1
u1 = @(t) (sin(pi*t)+1)*10;
% input 2
u2 = @(t) 5*(exp(-t)*t);

n = size(A,1);
H = sparse(Symmetrizing_Hessian(H,n)); % Symmeterize the Hessian for QB MOR

options1 = odeset('RelTol',1e-10,'AbsTol',1e-10);
options2 = odeset('RelTol',1e-10,'AbsTol',1e-10);
dx1  = @(t,x,A,H,N,b) A*x + H*kron(x,x) + N*x*u1(t) + b*u1(t);
dx2  = @(t,x,A,H,N,b) A*x + H*kron(x,x) + N*x*u2(t) + b*u2(t);
Err1 = [];
Err2 = [];

dxnl1 = @(t,x,A,B) A*x + B*u1(t) - x.^3;
dxnl2 = @(t,x,A,B) A*x + B*u2(t) - x.^3;

dxrd1 = @(t,x,A,T,B) A*x + B*u1(t) + T*kron(kron(x,x),x);
dxrd2 = @(t,x,A,T,B) A*x + B*u2(t) + T*kron(kron(x,x),x);

dxrcur1 = @(t,x,A,B,P,Vr) A*x + B*u1(t) + P*((Vr*x).^3);
dxrcur2 = @(t,x,A,B,P,Vr) A*x + B*u2(t) + P*((Vr*x).^3);

%% Original System Simulations
disp('Running full-order model')
[~,x1o] = ode15s(dxnl1,t,sparse(k,1),options1,Ad,Bd);
y1o = c(1:k)*x1o';
figure(1)
plot(t,y1o,'k')
hold all

[~,x2o] = ode15s(dxnl2,t,sparse(k,1),options2,Ad,Bd);
y2o = c(1:k)*x2o';
figure(2)
plot(t,y2o,'k')
hold all

%% Loewner Nonlinear Chafee Infante
r = 10; % Order of reduced system
disp('Computing ROM using Polynomial Loewner Approach')

[Edr, Adr,Tdr, Bdr, Cdr, T_full, V,W,Sigma] = PolyLoew_ExampleChafee(speye(k),Ad,Bd,c(1:k),r);

tic
[~,x1dr] = ode15s(dxrd1,t,zeros(r,1),options1,Adr,Tdr,Bdr);
y1dr = Cdr*x1dr';
time1.cubic = toc;

tic
[~,x2dr] = ode15s(dxrd2,t,zeros(r,1),options2,Adr,Tdr,Bdr);
y2dr = Cdr*x2dr';
time2.cubic = toc;

errd1 = abs((y1o-y1dr))./y1o;
errd2 = abs((y2o-y2dr))./y2o;
errd1(isnan(errd1)) = 0;
errd2(isnan(errd2)) = 0;

figure(1)
plot(t,y1dr,'m')
figure(2)
plot(t,y2dr,'m')
figure(3)
semilogy(t,errd1,'m')
hold on
figure(4)
semilogy(t,errd2,'m')
hold on

%% Loewner-Type MOR for QB systems
disp('Computing quadratic-bilinear ROM')
[Er, Ar, Hr, Nr, Br, Cr,SigmaQB] = QuadLoew_ROM(E,A,H,N,b,c,r);

tic
[~,x1r] = ode15s(dx1,t,zeros(r,1),options1,Ar,Hr,Nr,Br);
y1r = Cr*x1r';
time1.QB = toc;

figure(1)
plot(t,y1r,'r-')
hold all
legend('Original', 'PolyLoew', 'QuadBi')

% 
tic
[~,x2r] = ode15s(dx2,t,zeros(r,1),options2,Ar,Hr,Nr,Br);
y2r = Cr*x2r';
time2.QB = toc;

figure(2)
plot(t,y2r,'r-')
hold all
legend('Original', 'PolyLoew', 'QuadBi')

figure(3) 
semilogy(t,abs(y1r-y1o)./y1o,'r-')
hold all
ylim([1e-9,1])
legend('PolyLoew', 'QuadBi')

figure(4) 
semilogy(t,abs(y2r-y2o)./y2o,'r-')
hold all
ylim([1e-12,1])
legend('PolyLoew', 'QuadBi')

figure(5)
hold off
semilogy(sum(Sigma)/Sigma(1,1))
hold all
semilogy(sum(SigmaQB)/SigmaQB(1,1))
legend('PolyLoew', 'QuadBi')
xlim([0,50])
