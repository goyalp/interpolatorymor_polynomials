close all
clearvars
clc

addpath(genpath('./Compute_CUR_factorization/'))
k = 500;
l = 1;
rng(1)

[E,A,H,N,b,c] = ChafeeInfante_Matrices_zero_ic(k,l);
[Ad,Bd] = ChafeeInfante_POD_Matrices(k,l);

t = linspace(0,4,500);
% input 1
u1 = @(t) (sin(pi*t)+1)*10;
% input 2
u2 = @(t) 5*(exp(-t)*t);

n = size(A,1);
H = sparse(Symmetrizing_Hessian(H,n)); % Symmeterize the Hessian for QB MOR

options1 = odeset('RelTol',1e-10,'AbsTol',1e-10);
options2 = odeset('RelTol',1e-10,'AbsTol',1e-10);
dx1  = @(t,x,A,H,N,b) A*x + H*kron(x,x) + N*x*u1(t) + b*u1(t);
dx2  = @(t,x,A,H,N,b) A*x + H*kron(x,x) + N*x*u2(t) + b*u2(t);
Err1 = [];
Err2 = [];

dxnl1 = @(t,x,A,B) A*x + B*u1(t) - x.^3;
dxnl2 = @(t,x,A,B) A*x + B*u2(t) - x.^3;

dxrd1 = @(t,x,A,T,B) A*x + B*u1(t) + T*kron(kron(x,x),x);
dxrd2 = @(t,x,A,T,B) A*x + B*u2(t) + T*kron(kron(x,x),x);

dxrcur1 = @(t,x,A,B,P,Vr) A*x + B*u1(t) + P*((Vr*x).^3);
dxrcur2 = @(t,x,A,B,P,Vr) A*x + B*u2(t) + P*((Vr*x).^3);

%% Original System Simulations
disp('Simulating full-order model')
[~,x1o] = ode15s(dxnl1,t,sparse(k,1),options1,Ad,Bd);
y1o = c(1:k)*x1o';
figure(1)
plot(t,y1o,'k')
hold all

[~,x2o] = ode15s(dxnl2,t,sparse(k,1),options2,Ad,Bd);
y2o = c(1:k)*x2o';
figure(2)
plot(t,y2o,'k')
hold all

%% Loewner Nonlinear Chafee Infante
disp('Computing ROMs using two-sided and its CUR approximation')
r = 10; % Order of reduced system

[~, Adr,Tdr, Bdr, Cdr, T_full, V,W] = PolyLoew_ExampleChafee(speye(k),Ad,Bd,c(1:k),r);
[~,x1dr] = ode15s(dxrd1,t,zeros(r,1),options1,Adr,Tdr,Bdr);
y1dr = Cdr*x1dr';
  
[~,x2dr] = ode15s(dxrd2,t,zeros(r,1),options2,Adr,Tdr,Bdr);
y2dr = Cdr*x2dr';

errd1 = abs((y1o-y1dr))./y1o;
errd2 = abs((y2o-y2dr))./y2o;
errd1(isnan(errd1)) = 0;
errd2(isnan(errd2)) = 0;

figure(1)
plot(t,y1dr,'m')

figure(2)
plot(t,y2dr,'m')

figure(3)
semilogy(t,errd1,'m')
hold on

figure(4)
semilogy(t,errd2,'m')
hold on

%% Approximation with CUR of Cubic Term
[output] = CUR_Matrix(T_full);
col = output.col;
row = output.row;
U = output.Umat;
ApproxErro = norm(T_full - col*U*row)/norm(T_full);
indc = output.cidx{end}; % Determine the selected columns
indr = output.ridx{end}; % Determine the selected rows

P = -W'*col*U; % Pre-compute the matrix like in DEIM
Vr = V(indr,:); % Extract the matrix, corresponding to rows


[~,x1cur] = ode15s(dxrcur1,t,zeros(r,1),options1,Adr,Bdr,P,Vr);
y1cur = Cdr*x1cur';
  
[~,x2cur] = ode15s(dxrcur2,t,zeros(r,1),options2,Adr,Bdr,P,Vr);
y2cur = Cdr*x2cur';

errd1 = abs((y1o-y1cur))./y1o;
errd2 = abs((y2o-y2cur))./y2o;
errd1(isnan(errd1)) = 0;
errd2(isnan(errd2)) = 0;

figure(1)
plot(t,y1cur,'--b')

figure(2)
plot(t,y2cur,'--b')

figure(3)
semilogy(t,errd1,'--b')

figure(4)
semilogy(t,errd2,'--b')

%% One-sided Loewner Nonlinear Chafee Infante
disp('Computing ROMs using one-sided and its CUR approximation')
r = 10; % Order of reduced system
[Edr, Adr,Tdr, Bdr, Cdr, T_full, V,W] = PolyLoew_ExampleChafee_Onesided(speye(k),Ad,Bd,c(1:k),r);
[~,x1dr] = ode15s(dxrd1,t,zeros(r,1),options1,Adr,Tdr,Bdr);
y1dr = Cdr*x1dr';
  
[~,x2dr] = ode15s(dxrd2,t,zeros(r,1),options2,Adr,Tdr,Bdr);
y2dr = Cdr*x2dr';

errd1 = abs((y1o-y1dr))./y1o;
errd2 = abs((y2o-y2dr))./y2o;
errd1(isnan(errd1)) = 0;
errd2(isnan(errd2)) = 0;

figure(1)
plot(t,y1dr,'*m')

figure(2)
plot(t,y2dr,'*m')

figure(3)
semilogy(t,errd1,'*m')

figure(4)
semilogy(t,errd2,'*m')
%% Approximation with CUR of Cubic Term
[output] = CUR_Matrix(T_full);
col = output.col;
row = output.row;
U = output.Umat;
ApproxErro = norm(T_full - col*U*row)/norm(T_full);
indc = output.cidx{end}; % Determine the selected columns
indr = output.ridx{end}; % Determine the selected rows

P = -W'*col*U; % Pre-compute the matrix like in DEIM
Vr = V(indr,:); % Extract the matrix, corresponding to rows


[~,x1cur] = ode15s(dxrcur1,t,zeros(r,1),options1,Adr,Bdr,P,Vr);
y1cur = Cdr*x1cur';
  
[~,x2cur] = ode15s(dxrcur2,t,zeros(r,1),options2,Adr,Bdr,P,Vr);
y2cur = Cdr*x2cur';

errd1 = abs((y1o-y1cur))./y1o;
errd2 = abs((y2o-y2cur))./y2o;
errd1(isnan(errd1)) = 0;
errd2(isnan(errd2)) = 0;

figure(1)
plot(t,y1cur,'--*b')
legend('Orig model','Two-sided','Two-sided + CUR','One-sided','One-sided + CUR')
xlabel('Time')
ylabel('Response')

figure(2)
plot(t,y2cur,'--*b')
legend('Orig model','Two-sided','Two-sided + CUR','One-sided','One-sided + CUR')
xlabel('Time')
ylabel('Response')

figure(3)
semilogy(t,errd1,'--*b')
ylim([1e-10,1])
legend('Two-sided','Two-sided + CUR','One-sided','One-sided + CUR')
xlabel('Time')
ylabel('Relative error')

figure(4)
semilogy(t,errd2,'--*b')
ylim([1e-12,1])
legend('Two-sided','Two-sided + CUR','One-sided','One-sided + CUR')
xlabel('Time')
ylabel('Relative error')

