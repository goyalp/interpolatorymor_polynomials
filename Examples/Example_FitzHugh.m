close all
clearvars
clc

% Building model
k = 100;
l = 0.1;
[E,A,H,N,b,c] = Fitz_Matrices_POD(k,l);

A = E\A; 
H = E\H;
N = E\N;
b = E\b; E = E\E;

t = linspace(0,20,500);

w1 = @(t) 5*10^4 * t^3* exp(-15*t);  f1 = 1; % Two inputs
w2 = @(t) 10*(sin(2*pi*t)-2); f2 = 1; % Two inputs

rng(1)
n = size(A,1);
H = Symmetrizing_Hessian(H,n);

Ad = A(1:2*k,1:2*k);
Bd = b(1:2*k,:);
Cd = c(:,1:2*k);

dxnl1   = @(t,x,A,b) A*x + Fitz_NL(x)  +  b*[w1(t);f1];
dxnl2   = @(t,x,A,b) A*x + Fitz_NL(x)  +  b*[w2(t);f2];

dx1     = @(t,x,A,H,N,b) A*x + H*kron(x,x) + N*x*w1(t) + b*[w1(t);1];
dx2     = @(t,x,A,H,N,b) A*x + H*kron(x,x) + N*x*w2(t) + b*[w2(t);1];

dxrd1   = @(t,x,A,H,T,B) A*x + B*[w1(t);f1] + H*kron(x,x) + T*kron(kron(x,x),x);
dxrd2   = @(t,x,A,H,T,B) A*x + B*[w2(t);f2] + H*kron(x,x) + T*kron(kron(x,x),x);

dxcur1  = @(t,x,A,B, Pq,Vq,Pc,Vc) A*x + B*[w1(t);f1] + Pq*(Vq*x).^2 +  Pc*(Vc*x).^3;
dxcur2  = @(t,x,A,B, Pq,Vq,Pc,Vc) A*x + B*[w2(t);f1] + Pq*(Vq*x).^2 +  Pc*(Vc*x).^3;

options = odeset('RelTol',1e-10,'AbsTol',1e-10);

%% Original System Simulation
disp('Running full-order model')
tic
[~,x1] = ode15s(dxnl1,t,zeros(2*k,1),options,Ad,Bd);
y1 = Cd*x1';
time.full = toc;

figure(1)
plot(t(:,1:2:end),y1(:,1:2:end),'k')
xlabel('Time')
ylabel('Response')
hold all


%% Loewner-Type Approach  Two sided
disp('Computing ROM using Polynomial Loewner Approach (two-sided)')
r = 20;
[~, Adr, Hdr,Tdr, Bdr, Cdr] = PolyLoew_ExampleFHN(speye(2*k),Ad,Bd,Cd,r);

tic
[~,x1dr] = ode15s(dxrd1,t,zeros(r,1),options,Adr,Hdr,Tdr,Bdr);
y1dr = Cdr*x1dr';
time.NL = toc;

errd1 = mean(abs((y1-y1dr)./y1));
errd1(isnan(errd1)) = 0;

figure(1)
plot(t,y1dr,'m')

figure(3)
semilogy(t,errd1,'m')
xlabel('Time')
ylabel('Relative error')
hold all

%% Loewner-Type Approach One sided
disp('Computing ROM using Polynomial Loewner Approach (one-sided)')
[~, Adr, Hdr,Tdr, Bdr, Cdr,~,~,~, ~, Sigma_OS] = PolyLoew_ExampleFHN_Onesided(speye(2*k),Ad,Bd,Cd,r);

tic
[~,x1dr_OS] = ode15s(dxrd1,t,zeros(r,1),options,Adr,Hdr,Tdr,Bdr);
y1dr_OS = Cdr*x1dr_OS';
time.NL_OS = toc;


errd1 = mean(abs((y1-y1dr_OS)./y1));
errd1(isnan(errd1)) = 0;

figure(1)
plot(t,y1dr,'c')

figure(3)
semilogy(t,errd1,'c')

%% Loewner-Type MOR for QB systems
disp('Computing quadratic-bilinear ROM (one-sided)')
[~, Ar, Hr, Nr, Br, Cr,SigmaQB_OS] = QuadLoew_ROM_Onesided(E,A,H,N,b,c,r);

tic
[~,x1r] = ode15s(dx1,t,zeros(r,1),options,Ar,Hr,Nr,Br);
y1r = Cr*x1r';
time.QB_OS = toc;

errd1 = mean(abs((y1-y1r)./y1));
errd1(isnan(errd1)) = 0;

figure(1)
plot(t,y1r,'r-')

figure(3) 
semilogy(t,errd1,'r')
ylim([1e-12,1])

%% Loewner-Type MOR for QB systems
disp('Computing quadratic-bilinear ROM (two-sided)')

[Er, Ar, Hr, Nr, Br, Cr,SigmaQB] = QuadLoew_ROM_Twosided(E,A,H,N,b,c,r);
tic
[~,x1r] = ode15s(dx1,t,zeros(r,1),options,Ar,Hr,Nr,Br);
y1r = Cr*x1r';
time.QB = toc;
disp('  == Observe: the integrator fails!!')

% figure(1)
% plot(t,y1r,'g-')
% hold all
% 

% figure(3) 
% semilogy(t,errd1,'g')
% hold all
% ylim([1e-9,1])
% % figure(4) 
% % semilogy(t,abs(y2r-y2)./y2,'r-')
% hold all
% ylim([1e-12,1])

%% Loewner-Type Approach  Two sided
disp('Computing ROM using Polynomial Loewner Approach (two-sided, r = 6)')

r = 6;

[Edr, Adr, Hdr,Tdr, Bdr, Cdr,H_full,T_full,V, W, Sigma] = PolyLoew_ExampleFHN(speye(2*k),Ad,Bd,Cd,r);

tic
[~,x1dr] = ode15s(dxrd1,t,zeros(r,1),options,Adr,Hdr,Tdr,Bdr);
y1dr = Cdr*x1dr';
time.NL6 = toc ;

errd1 = mean(abs((y1-y1dr)./y1));
errd1(isnan(errd1)) = 0;

figure(1)
plot(t,y1dr,'y-')
legend('Orig Sys','','Poly\_Twosided (r=20)','','Poly\_Onesided (r=20)','','Quad\_Onesided (r=20)','','Poly\_Twosided(r=6)','')

figure(3)
semilogy(t,errd1,'y-')
legend('Poly\_Twosided (r=20)','Poly\_Onesided (r=20)','Quad\_Onesided (r=20)','Poly\_Twosided(r=6)')

%%
figure(5)
semilogy(sum(Sigma)/Sigma(1,1))
hold all
semilogy(sum(Sigma_OS)/Sigma_OS(1,1))
semilogy(sum(SigmaQB)/SigmaQB(1,1))
semilogy(sum(SigmaQB_OS)/SigmaQB_OS(1,1))
legend('Poly\_Twosided', 'Poly\_Onesided', 'Quad\_Twosided','Quad\_Onesided')