function [y] = Fitz_NL(x)
% state-nonlinear function f for RC-Circuit
n = length(x);
k = round(n/2);

y = 1.1*(200/3)*[x(1:k).^2;sparse(k,1)] - (200/3)*[x(1:k).^3;sparse(k,1)];
end