function [Er, Ar, Tr, Br, Cr, T_full,V, W,Se] = PolyLoew_ExampleChafee(E,A,B,C,r)

n = size(E,1);
s = logspace(-3,3,200);
freq = 1i*s;

V = [];
W = [];

fprintf('  == Computing Projection Matrix: iteration: #       ');
for i = 1:length(freq)
    fprintf('\b\b\b\b\b\b\b\b')
    fprintf('%3d/%3d\n', i,length(freq));
    
    xl = (freq(i)*E-A)\B;
    yl = (freq(i)*E-A)'\C';
    xh = (freq(i)*E-A)\((xl.^3));
    yh = (freq(i)*E-A)'\( (diag(xl.^2))'*yl );
       
    xh = sparse(xh);
    yh = sparse(yh);
    
    V = [V real(xl),imag(xl), real(xh), imag(xh)];
    W = [W real(yl),imag(yl), real(yh), imag(yh)];
end

Ek = W'*E*V;
Ak = W'*A*V;


fprintf('  == Computing SVD of Loewner and Shifted Loewner matrices\n');
[Ue,Se,~] = svds([Ek,Ak],100);
[~,Sa,Va] = svds([Ek;Ak],100);

Yk = Ue(:,1:r);
Xk = Va(:,1:r);

V = V*Xk;
W = W*Yk;

% [V,~] = qr(V,0);
% [W,~] = qr(W,0);

fprintf('  == Computing ROM matrices\n');

Er = W'*V;
Ar = W'*A*V; Ar = Er\Ar;
Br = W'*B;   Br = Er\Br;
Cr = C*V;

 for i = 1:n
     Tr(i,:) = kron(kron(V(i,:), V(i,:)), V(i,:));
 end
 T_full = -Tr;
 Tr = W'*Tr;
 Tr = -Er\Tr;
 W = (Er\(W'))';
 Er = Er\Er;




