function [Er, Ar, Hr,Tr, Br, Cr,H_full,T_full,V, W,Se] = PolyLoew_ExampleFHN_Onesided(E,A,B,C,r)

n = size(E,1);
s = logspace(-2,2,200);
freq = 2i*pi*s;
V = [];
W = [];
k = ceil(n/2);
m = size(B,2);
p = size(C,1);
p0 = zeros(k,1);
fprintf('  == Computing Projection Matrix: iteration: #       ');
for i = 1:length(freq)
    fprintf('\b\b\b\b\b\b\b\b')
    fprintf('%3d/%3d\n', i,length(freq));
    % Linear Term
    xl = (freq(i)*E-A)\(B*(randn(m,1) + sqrt(-1)*randn(m,1)));
    yl = (freq(i)*E-A)'\(C'*(randn(p,1) + sqrt(-1)*randn(p,1)));
    
    xlk = xl(1:k); ylk = yl(1:k);
    
    % Quadratic Term
    ph = [xlk;p0];
    xh = (freq(i)*E-A)\(ph.^2);
    qh = (diag(ph))'*[ylk;p0];
    yh = (freq(i)*E-A)'\qh;
    xh = sparse(xh);
    yh = sparse(yh);
    % Cubic Term
    xc = (freq(i)*E-A)\(ph.^3);
    qc = (diag(ph.^2))'*[ylk;p0];
    yc = (freq(i)*E-A)'\qc;
    
    xc = sparse(xc);
    yc = sparse(yc);
    
    V = [V real(xl),imag(xl), real(xh), imag(xh),real(xc), imag(xc)];
    %W = [W real(yl),imag(yl), real(yh), imag(yh),real(yc), imag(yc)];
end
W = V;
Ek = W'*E*V;
Ak = W'*A*V;

fprintf('  == Computing SVD of Loewner and Shifted Loewner matrices\n');
[Ue,Se,Ve] = svds([Ek,Ak],50);
[Ua,Sa,Va] = svds([Ek;Ak],50);

Yk = Ue(:,1:r);
Xk = Va(:,1:r);

V = V*Xk;
W = W*Yk;
[V,~] = qr(V,0);
[W,~] = qr(W,0);
W = V;
fprintf('  == Computing ROM matrices\n');

Er = W'*V;
Ar = W'*A*V; Ar = Er\Ar;
Br = W'*B; Br = Er\Br;
Cr = C*V;

 for i = 1:k
     Tr(i,:) = (200/3)*kron(kron(V(i,:), V(i,:)), V(i,:));
 end
 Tr = [Tr;zeros(k,r^3)];
 T_full = Tr;
 Tr = W'*Tr;
 Tr = -Er\Tr;
 
  for i = 1:k
     Hr(i,:) = 1.1*(200/3)*kron(V(i,:), V(i,:));
  end
 Hr = [Hr;zeros(k,r^2)];
 H_full = Hr;
 Hr = W'*Hr;
 Hr = Er\Hr;

 W = (Er\(W'))';
 Er = Er\Er;




