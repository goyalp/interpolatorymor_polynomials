function [Er, Ar, Hr, Nr, Br, Cr,Se] = QuadLoew_ROM(E,A,H,N,B,C,r)

n = size(E,1);
H = Symmetrizing_Hessian(H,n);
Htensor = ndSparse(H,[n,n,n]);
H2 = reshape(permute(Htensor,[2 1 3]),n,n^2); 


s = logspace(-3,3,200);
freq = 1i*s;

V = [];
W = [];
m = size(B,2);
p = size(C,1);
Vc = [];
Wc = [];

fprintf('  == Computing Projection Matrix: iteration: #       ');
for i = 1:length(freq)
    fprintf('\b\b\b\b\b\b\b\b')
    fprintf('%3d/%3d\n', i,length(freq));
    xl = (freq(i)*E-A)\(B*(randn(m,1) + sqrt(-1)*randn(m,1)));
    yl = (freq(i)*E-A)'\(C'*(randn(p,1) + sqrt(-1)*randn(p,1)));
    xh = (freq(i)*E-A)\(H*kron(xl,xl));
    yh = (freq(i)*E-A)'\(H2*kron(xl,yl));
    xn = (freq(i)*E-A)\(N*xl);
    yn = (freq(i)*E-A)'\(N'*yl);
    
    xh = sparse(xh);
    yh = sparse(yh);
    
    Vc = [Vc xl,conj(xl), xh, conj(xh), xn, conj(xn)];
    Wc = [Wc yl,conj(yl), yh, conj(yh),yn, conj(yn)];
    V = [V real(xl),imag(xl), real(xh), imag(xh), real(xn), imag(xn)];
    W = [W real(yl),imag(yl), real(yh), imag(yh), real(yn), imag(yn)];
end

Ek = W'*E*V;
Ak = W'*A*V;


fprintf('  == Computing SVD of Loewner and Shifted Loewner matrices\n');
[Ue,Se,Ve] = svds(sparse(Ek),50);
[Ua,Sa,Va] = svds(sparse([Ek;Ak]),50);

Yk = Ue(:,1:r);
Xk = Ve(:,1:r);

V1 = V*Xk;
W1 = W*Yk;
[V1,~] = qr(V1,0);
[W1,~] = qr(W1,0);

fprintf('  == Computing ROM matrices\n');
Er = W1'*E*V1;
Ar = Er\(W1'*A*V1);
Nr = Er\(W1'*N*V1);
Br = Er\(W1'*B);
Cr = C*V1;
Hr = sparse(W1'*H*kron(V1,V1));
Hr = Er\Hr;
Er = Er\Er;

