function [Er, Ar, Hr, Nr, Br, Cr,Se,freq] = QuadLoew_ROM_Twosided(E,A,H,N,B,C,r)

n = size(E,1);
H = Symmetrizing_Hessian(H,n);
Htensor = ndSparse(H,[n,n,n]);
H2 = reshape(permute(Htensor,[2 1 3]),n,n^2); 

%keyboard
s = logspace(-2,2,200);
freq = 1i*s;
freq_r = freq(1:1:end);
freq_l = freq(1:1:end);

V = [];
W = [];
m = size(B,2);
p = size(C,1);
%Vc = [];
%Wc = [];
Tb = [];
Tc = [];
fprintf('  == Computing Projection Matrix: iteration: #       ');
for i = 1:length(freq)
    fprintf('\b\b\b\b\b\b\b\b')
    fprintf('%3d/%3d\n', i,length(freq));
    tb = randn(m,1) + sqrt(-1)*randn(m,1);
    tc = randn(p,1) + sqrt(-1)*randn(p,1);
    Tb = [Tb, tb];
    Tc = [Tc,tc];
    xl = (freq_r(i)*E-A)\(B*tb); xl = xl/norm(xl);
    yl = (freq_l(i)*E-A)'\(C'*tc); yl = yl/norm(yl);
    xh = (freq_r(i)*E-A)\(H*kron(xl,xl));xh = xh/norm(sparse(xh));
    t =   H*kron(speye(size(A)),xl); 
    yh = (freq_l(i)*E-A)'\(t'*yl); yh = yh/norm(sparse(yh));
    xn = (freq_r(i)*E-A)\(N*xl);xn = xn/norm(xn);
    yn = (freq_l(i)*E-A)'\(N'*yl); yn = yn/norm(yn);
    xh = sparse(xh);
    yh = sparse(yh);
    
    V = [V real(xl),imag(xl), real(xh), imag(xh), real(xn), imag(xn)];
    W = [W real(yl),imag(yl), real(yh), imag(yh), real(yn), imag(yn)];
end

V(isnan(V))=0;
W(isnan(W))=0;
Ek = W'*E*V;
Ak = W'*A*V;

fprintf('  == Computing SVD of Loewner and Shifted Loewner matrices\n');
[Ue,Se,Ve] = svds(sparse([Ek,Ak]),100);
[Ua,Sa,Va] = svds(sparse([Ek;Ak]),100);

Yk = Ue(:,1:r);
Xk = Va(:,1:r);

V1 = V*Xk;
W1 = W*Yk;
[V1,~] = qr(V1,0);
[W1,~] = qr(W1,0);

fprintf('  == Computing ROM matrices\n');
Er = W1'*E*V1;
Ar = Er\(W1'*A*V1);
Nr = Er\(W1'*N*V1);
Br = Er\(W1'*B);
Cr = C*V1;
Hr = sparse(W1'*H*kron(V1,V1));
Hr = Er\Hr;
Er = Er\Er;

