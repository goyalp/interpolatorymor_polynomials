function symH = Symmetrizing_Hessian(H,n)

Htensor = ndSparse(H,[n,n,n]);
H2 = reshape(permute(Htensor,[2 1 3]),n,n^2);
H3 = reshape(permute(Htensor,[3 1 2]),n,n^2);

symH2 = 0.5*(H2 + H3);
symHtensor2 = reshape(symH2,n,n,n);
symHtensor = permute(symHtensor2,[2 1 3]);
symH = reshape(symHtensor,n,n^2);