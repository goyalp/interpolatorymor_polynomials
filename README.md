## Construction of Interpolatory Reduced Systems for Polynomial Systems

This repository contains a MATLAB/OCTAVE implementation of the algorithm proposed in the paper "Interpolation-based model order reduction for polynomial systems"[1] to construct a reduced-order model for polynomial systems preserving the polynomial structure in the reduced-order model.
<center>
<img src="./Img/FHN_simulations.png", width="600", height="300" />
</center>

## Overview of the Approach
We consider the systems of the form:
<center>
<p align="center"><img src="./Img/Poly_eqn.png" 
alt=""
 height=120/></p>
 </center>
Our goal is to construct interpolatory reduced-order systems while preserving the polynomial structure. 

## Examples 
The `Examples` folder contains two examples that are considered in the paper [[1]](https://arxiv.org/pdf/1904.11891.pdf). We have efficiently implemented each example by making use of the particular structure of polynomial nonlinearity. Please run ```Example_Chafee.m``` and ```Example_FitzHugh.m```. Also, there is a script ```Example_Chafee_CUR.m``` that makes use of the CUR matrix decomposition to futher speed up the reduced-order systems simulations. 

## Cite As
Please cite the paper [[1]](https://arxiv.org/pdf/1904.11891.pdf) if you use the algorithm and implementation in your research. 
<details><summary> BibTeX </summary><pre>
@TechReport{morBenG19,
author = {Benner, P. and Goyal, P.},
title = {Interpolation-Based Model Order Reduction for Polynomial
Parametric Systems},
institution = {arXiv},
year = 2019,
type = {e-prints},
number = {1904.11891},
note = {math.NA},
url = {https://arxiv.org/abs/1904.11891}
}
}</pre></details>




## References
[1]. Benner, P. and Goyal, P., Interpolation-Based Model Order Reduction for Polynomial Systems, arXiv:1904.11891, 2019.
___

This script has been written in MATLAB 2016. We have downloaded the package for the CUR matrix decomposition from [https://github.com/SimonDu/CUR-matrix-decomposition](https://github.com/SimonDu/CUR-matrix-decomposition). Please contact [Pawan Goyal](mailto:goyalp@mpi-magdeburg.mpg.de) for any queries and comments. 









